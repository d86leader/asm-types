#pragma once

struct Node
{
    int val;
    Node* next;
};


void add_to_all(Node* node, int val);
