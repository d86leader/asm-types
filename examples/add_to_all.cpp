#include "node.h"

void add_to_all(Node* node, int val)
{
    while (node != nullptr)
    {
        node->val += val;
        node = node->next;
    }
}
