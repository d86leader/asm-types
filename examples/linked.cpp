#include <iostream>
#include "node.h"

void log_all(Node* node)
{
    while (node != nullptr)
    {
        std::cout << node->val;
        node = node->next;
        if (node != nullptr)  std::cout << " >> ";
    }
    std::cout << std::endl;
}

int main()
{
    Node node2 {.val=10, .next=nullptr};
    Node node1 {.val=5, .next=&node2};
    Node* list = &node1;
    log_all(list);
    add_to_all(list, 100);
    log_all(list);

    return 0;
}
