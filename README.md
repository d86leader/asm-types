# Asm Types

Paper: https://gitlab.com/d86leader/asm-types-thesis

## Building

Get [haskell stack](https://docs.haskellstack.org/en/stable/README/)

Then build with `stack build` and run with `stack run`.
Currently it builds and runs demo inference with debug information.
