{-# LANGUAGE DeriveFunctor, DeriveFoldable, BangPatterns, LambdaCase #-}
{-# LANGUAGE OverloadedLists #-}
module Main where

import Data.Vector (fromList)

import Construction
import Infer hiding (Deref)


linkedList :: AsmType
linkedList = Mu "X" $ Struct [TInt 64, TPtr $ TTypeVar "X"]

doubleLinkedList :: AsmType
doubleLinkedList = Mu "X" $ Struct [TPtr $ TTypeVar "X", TInt 64, TPtr $ TTypeVar "X"]

ptrToLinkedList :: AsmType
ptrToLinkedList =
    Mu "X" $ TPtr $ Struct [TPtr $ TTypeVar "X", TInt 64]

linkedListOfLinkedLists :: AsmType
linkedListOfLinkedLists =
    Mu "X" $ Struct [ TPtr $ TTypeVar "X", Mu "Y" $ Struct [TPtr $ TTypeVar "Y", TInt 64] ]


-- rsi - linked list of ints
-- rdi - what to add to each element
linkedListAsm :: SimpleTerm
linkedListAsm =
    let
      rbp =      Reg "rbp"
      rsp = RW $ Reg "rsp"
      rdi = RW $ Reg "rdi"
      derefRsi pos = Deref $ ModRM (pos * 8) Nothing "rsi" 1 8
      program = fromList
        [ CPush         (RW rbp)
        , CMov          rbp  rsp
        , CAdd          (derefRsi 0) rdi
        , CMov          (Reg "rsi")  (RW $ derefRsi 1)
        , CJz           2 -- two down
        , CJmp          (negate 3) -- three up, cycle start
        , CPop          rbp
        , CRet
        ]
    in SimpleTerm program 0 (RW $ Reg "rsi")

linkedListOpt :: SimpleTerm
linkedListOpt =
    let
      derefRdi pos = Deref $ ModRM (pos * 8) Nothing "rdi" 1 8
      program = fromList
        [ CJmp           4 -- four down
        , CNullary "nop"
        , CAdd           (derefRdi 0)     (RW $ Reg "esi")
        , CMov           (Reg "rdi")      (RW $ derefRdi 1)
        , CBinary "test" (RW $ Reg "rdi") (RW $ Reg "rdi")
        , CJz            (negate 4) -- four up
        , CRet
        ]
    in SimpleTerm program 0 (RW $ Reg "rdi")


linkedListCppBasic :: SimpleTerm
linkedListCppBasic =
    let
      rax = Reg "rax"
      rbp = Reg "rbp"
      rsp = RW $ Reg "rsp"
      rdx = Reg "rdx"
      local off = Deref $ ModRM (negate off) Nothing "rbp" 1 8
      program = fromList
        [ CPush  (RW rbp)
        , CMov  rbp  rsp
        , CMov  (local 8)  (RW $ Reg "rdi")
        , CMov  (local 0xc)  (RW $ Reg "rsi")
        , CBinary "cmp"  (RW $ local 8)  (Imm 0)
        , CJz 11
        , CMov  rax  (RW $ local 8)
        , CMov  rdx  (RW $ Deref $ ModRM 8 Nothing "rax" 1 0)
        , CMov  rax  (RW $ local 0xc)
        , CAdd  rdx  (RW rax)
        , CMov  rax  (RW $ local 8)
        , CMov  (Deref $ ModRM 8 Nothing "rax" 1 0)  (RW rdx)
        , CMov  rax  (RW $ local 8)
        , CMov  rax  (RW $ Deref $ ModRM 0 Nothing "rax" 1 8)
        , CMov  (local 8)  (RW rax)
        , CJmp  (negate 11)
        , CNullary "nop"
        , CPop  rbp
        , CRet
        ]
--     in SimpleTerm program 0 (RW $ Reg "rsi")
    in SimpleTerm program 0 (RW $ Reg "rdi")


debugType :: AsmType -> String
debugType t =
    show t <> "; " <> show (coherentType t)


main :: IO ()
main = do
    putStrLn . debugType $ linkedList
    putStrLn . debugType $ ptrToLinkedList
    putStrLn . debugType $ doubleLinkedList
    putStrLn . debugType $ linkedListOfLinkedLists
    --
    putStrLn "---\nExample one:"
    print linkedListAsm
    putStrLn . show . treeAsType $ buildTree linkedListAsm 0
    --
    putStrLn "---\nExample two:"
    print linkedListCppBasic
    putStrLn . show . treeAsType $ buildTree linkedListCppBasic 0
    --
    putStrLn "---\nExample three:"
    print linkedListOpt
    putStrLn . show . treeAsType $ buildTree linkedListOpt 0
