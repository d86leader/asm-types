{-# LANGUAGE DeriveFunctor, DeriveFoldable, FlexibleInstances #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes, GADTs #-}
-- | Define terms and types, from my math constructions to ADTs
--
-- Semantics of 'ExecutionStep' parameter: at the state of machine /before/
-- command on the line was run. So ('push rbp', rsp, 1) means push hasn't happened yet
module Construction
    -- * Terms
    ( RegName, CommandName, Immediate, ModRM (..), RWOperand (..)
    , Operand (..), Command (..), Program, ExecutionStep
    , SimpleTerm (..)
    , TermExpr (..), Term (..), simpleTerm, derefModRm, derefModRmG
    -- * Types
    , TypeVar
    , Incoherent (..), coherentType
    , AsmType (..)
    -- * Semantics of commands
    , CommandInfo (..), commandInfo
    -- * Utility
    , showProgram
    ) where

import Data.Hashable (Hashable)
import Data.List (intersperse)
import Data.List.NonEmpty (NonEmpty)
import Data.Maybe (catMaybes)
import Data.MonoTraversable (olength, ofoldMap)
import Data.Vector (Vector)
import GHC.Generics (Generic)

import qualified Data.List.NonEmpty as NE
import qualified Data.Vector as V
import qualified Numeric as Num

import Prelude hiding (lines)


type RegName = String
type CommandName = String
type ExecutionStep = Int

type Immediate = Int
-- | x86 dereference
data ModRM = ModRM { constOffset :: Int
                   , regOffset   :: Maybe RegName
                   , regElem     :: RegName
                   , elemSize    :: Int
                   , operSize    :: Int
                   }
    deriving (Eq, Show, Generic)

instance Hashable ModRM

data RWOperand = Reg   RegName
               | Deref ModRM
    deriving (Eq, Show, Generic)

instance Hashable RWOperand

data Operand = RW  RWOperand
             | Imm Immediate
    deriving (Eq)

data Command
    = CMov RWOperand Operand
    | CAdd RWOperand Operand
    | CPush Operand
    | CPop RWOperand
    | CJmp Immediate -- why not function jumps?
    | CJz  Immediate
    | CRet
    | CNullary CommandName
    | CUnary   CommandName Operand
    | CBinary  CommandName Operand Operand
    deriving (Eq)

type Program = Vector Command

-- | Not well-formed, as it allows operands not from context, but w/e
data SimpleTerm = SimpleTerm Program ExecutionStep Operand
    deriving (Eq)


-- | This does not structurally include 'Operand' - it omits modrm, becase we
-- can construct modrm with addition, multiplication and deref

data TermExpr
    = TReg    RegName ExecutionStep
    | Number  Immediate
    | TDeref  TermExpr Int -- ^ second is size
    | TermExpr :+: TermExpr
    | TermExpr :*: TermExpr
    deriving (Eq, Generic)

instance Hashable TermExpr

-- | Not well-formed, as it allows operands not from context, but w/e
data Term = Term Program TermExpr
    deriving (Eq)

simpleTerm :: SimpleTerm -> Term
simpleTerm (SimpleTerm program step op) = case op of
    Imm imm -> Term program (Number imm)
    RW (Reg reg) -> Term program (TReg reg step)
    RW (Deref modrm) -> Term program $ derefModRm step modrm

-- | Expression that is identical to dereferencing given modrm value
derefModRm :: ExecutionStep -> ModRM -> TermExpr
derefModRm step =
    let deref size e = TDeref e size
        plus = (:+:)
        mul = (:*:)
        reg name = TReg name step
        imm = Number
    in derefModRmG deref plus mul reg imm

derefModRmG
    :: (Int -> e -> e2) -- ^ deref
    -> (e -> e -> e) -- ^ plus
    -> (e -> e -> e) -- ^ multiply
    -> (RegName -> e)
    -> (Immediate -> e)
    -> ModRM -> e2
derefModRmG deref plus mul reg imm
            ModRM {constOffset, regOffset, regElem, elemSize, operSize} =
    let
      mbConstOffset = if constOffset == 0 then Nothing else Just $ imm constOffset
      mbElem = case elemSize of
        0 -> Nothing
        1 -> Just $ reg regElem
        x -> Just $ reg regElem `mul` imm x
      mbRegOff = reg <$> regOffset
      summants = catMaybes [mbConstOffset, mbRegOff, mbElem]
    in deref operSize $ case summants of
        [] -> imm 0
        nonEmpty -> foldr1 plus nonEmpty


type TypeVar = String

data AsmType
    = TInt Int
    | TFloat Int
    | TUPtr
    | TFPtr
    | TPtr AsmType
    | Struct (NonEmpty AsmType)
    | Mu TypeVar AsmType
    | TTypeVar TypeVar


-- | Error type, describes why the type is incoherent
data Incoherent = DuplicateTypeVar TypeVar
                | UnboundTypeVar TypeVar
    deriving (Eq, Show)


coherentType :: AsmType -> [Incoherent]
coherentType = coherentType' []

coherentType' :: [TypeVar] -> AsmType -> [Incoherent]
coherentType' marks = \case
    Mu mark t ->
        if mark `elem` marks
        then (DuplicateTypeVar mark) : coherentType' marks t
        else coherentType' (mark:marks) t
    TTypeVar mark ->
        if mark `elem` marks
        then []
        else [UnboundTypeVar mark]
    TPtr t -> coherentType' marks t
    Struct ts -> ofoldMap (coherentType' marks) ts
    _ -> []


-- | Semantics of asm command
data CommandInfo = CommandInfo
    { equivalences :: [(TermExpr, TermExpr)] -- ^ basic equivalences found in line
    , entry :: ExecutionStep -- ^ Where we got to this command
    , exits :: [ExecutionStep] -- ^ Where execution can go after this
    , modifications :: [RWOperand] -- ^ Operands modified by line
    , dereferenced :: [ModRM]
    } deriving (Eq, Show)

commandInfo :: ExecutionStep -> Command -> CommandInfo
commandInfo step = \case
    -- mov: left operand on next step is equal to right on current
    CMov receiver from ->
        let from' = operandAsExpr step from
            newExpr = rwopAsExpr (step + 1) receiver
        in CommandInfo
            { equivalences = [(from', newExpr)]
            , entry = step
            , exits = [step + 1]
            , modifications = [receiver]
            , dereferenced = getDereferences (RW receiver) ++ getDereferences from
            }
    -- add: left operand on current step is equal to sum of current
    CAdd receiver from ->
        let item1 = operandAsExpr (step + 1) from
            item2 = rwopAsExpr step receiver
            sumExpr = item1 :+: item2
            recv' = rwopAsExpr (step + 1) receiver
        in CommandInfo
            { equivalences = [(sumExpr, recv')]
            , entry = step
            , exits = [step + 1]
            , modifications = [receiver]
            , dereferenced = getDereferences (RW receiver) ++ getDereferences from
            }
    CPush op -> CommandInfo
      { equivalences =
          [ (TReg "rsp" (step+1), TReg "rsp" step :+: Number (negate 8))
          , (TReg "rsp" (step+1) :+: Number 8, TReg "rsp" step)
          ]
      , entry = step
      , exits = [step + 1]
      , modifications = [Reg "rsp"]
      , dereferenced = getDereferences op
      }
    CPop receiver -> CommandInfo [] step [step + 1] [Reg "rsp", receiver]
                                 (getDereferences (RW receiver))
    -- jumps don't modify anything
    CJz next -> CommandInfo [] step [step + next, step + 1] [] []
    CJmp next -> CommandInfo [] step [step + next] [] []
    CRet -> CommandInfo [] step [] [] []
    -- we don't know anything about other commands
    _ -> CommandInfo [] step [step + 1] [] []
  where
    operandAsExpr :: ExecutionStep -> Operand -> TermExpr
    operandAsExpr _ (Imm imm)  = Number imm
    operandAsExpr step' (RW rw) = rwopAsExpr step' rw

    rwopAsExpr :: ExecutionStep -> RWOperand -> TermExpr
    rwopAsExpr step' (Reg name)    = TReg name step'
    rwopAsExpr step' (Deref modrm) = derefModRm step' modrm

getDereferences :: Operand -> [ModRM]
getDereferences = \case
    RW (Deref modrm) -> [modrm]
    _ -> []


--------------------------
-- Ord instances for terms

data ModRMGetter where
    ModRMGetter :: forall a. Ord a => (ModRM -> a) -> ModRMGetter
instance Ord ModRM where
    x1 `compare` x2 = selectNonEq . map applyCompare $
        [ ModRMGetter regOffset
        , ModRMGetter constOffset
        , ModRMGetter elemSize
        , ModRMGetter regElem
        , ModRMGetter operSize
        ]
      where
        selectNonEq l = case takeWhile (== EQ) l of
            x:_ -> x
            []  -> EQ
        applyCompare :: ModRMGetter -> Ordering
        applyCompare (ModRMGetter f) = f x1 `compare` f x2


instance Ord RWOperand where
    compare (Reg n1) (Reg n2) = compare n1 n2
    compare (Reg _) (Deref _) = LT
    compare (Deref _) (Reg _) = GT
    compare (Deref d1) (Deref d2) = compare d1 d2

instance Ord Operand where
    compare (RW x1) (RW x2) = compare x1 x2
    compare (Imm _) (RW _) = LT
    compare (RW _) (Imm _) = GT
    compare (Imm x1) (Imm x2) = compare x1 x2

instance Ord TermExpr where
    compare (Number x1) (Number x2) = compare x1 x2
    compare (Number _) _ = LT
    compare _ (Number _) = GT

    compare (TReg x1 s1) (TReg x2 s2) =
        compare (x1, s1) (x2, s2)
    compare (TReg _ _) _ = LT
    compare _ (TReg _ _) = GT

    compare (TDeref e1 sz1) (TDeref e2 sz2) =
        compare (e1, sz1) (e2, sz2)
    compare (TDeref _ _) _ = LT
    compare _ (TDeref _ _) = GT

    compare (e1l :+: e1r) (e2l :+: e2r) =
        compare (e1l, e1r) (e2l, e2r)
    compare (_ :+: _) _ = LT
    compare _ (_ :+: _) = GT

    compare (e1l :*: e1r) (e2l :*: e2r) =
        compare (e1l, e1r) (e2l, e2r)


-----------------
-- Show instances

instance Show Command where
    show = \case
        CMov op1 op2 -> "mov " <> show (RW op1) <> ", " <> show op2
        CAdd op1 op2 -> "add " <> show (RW op1) <> ", " <> show op2
        CJmp imm -> "jmp " <> show imm
        CJz  imm -> "jz "  <> show imm
        CPush op -> "push " <> show op
        CPop op -> "pop " <> show (RW op)
        CRet -> "ret"
        CNullary cmd -> cmd
        CUnary   cmd op -> cmd <> " " <> show op
        CBinary  cmd op1 op2 -> cmd <> " " <> show op1 <> ", " <> show op2

instance Show Operand where
    show = \case
        Imm val -> showHex val
        RW (Reg reg) -> reg
        RW (Deref modrm) -> derefModRmG
            (\size ex -> "[" <> ex <> "]_" <> show size)
            (\l r -> l <> " + " <> r)
            (\l r -> l <> " * " <> r)
            id
            show
            modrm

showHex :: (Integral a, Show a) => a -> String
showHex = flip Num.showHex ""

instance Show TermExpr where
    show = \case
        TReg reg step -> reg <> " at " <> show step
        Number imm -> showHex imm
        TDeref expr size ->
            "[" <> show expr <> "]_" <> show size
        expr1 :+: expr2 -> "(" <> show expr1 <> " + " <> show expr2 <> ")"
        expr1 :*: expr2 -> show expr1 <> " * " <> show expr2

showProgram :: Program -> String
showProgram v =
    let lines = olength v
        digits = ceiling . logBase 10 . asDouble $ lines + 1 -- +1 so round numbers work correctly
        line' = line digits
    in unlines . map line' . zip [0, 1 ..] . V.toList $ v
    where
        line :: Integer -> (Integer, Command) -> String
        line digits (n, comm) =
            let linenr = show n
                pad = (fromIntegral digits) - olength linenr
            in replicate pad ' ' <> linenr <> " | " <> show comm
        asDouble :: Integral a => a -> Double
        asDouble = fromIntegral

instance Show SimpleTerm where
    show (SimpleTerm program step operand) =
        "SimpleTerm program "
        <> show step
        <> " (" <> show operand <> ") where program =\n"
        <> showProgram program

instance Show AsmType where
    show = \case
        TInt s -> "Int" <> show s
        TFloat s -> "Float" <> show s
        TUPtr -> "UPtr"
        TFPtr -> "FPtr"
        TPtr t -> "Ptr " <> show t
        Mu v t -> "μ" <> v <> ". " <> show t
        TTypeVar v -> v
        Struct ts -> "Struct {" <> (concat . intersperse ", " . map show . NE.toList $ ts) <> "}"
