{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}
module Infer
    ( buildTree
    , Tree (..), Leaf (..), UsageTree (..), OpUsage (..)
    , treeAsType, treeAsTerm
    , _Terminal, _Deref
    , getSteps
    ) where

import Control.Applicative (Alternative, liftA2, (<|>))
import Control.Arrow ((***), (>>>), first, second)
import Control.Monad (guard, when, (>=>))
import Data.Bifunctor (bimap)
import Data.Containers (union, setToList, member, insertMap)
import Data.List.NonEmpty (NonEmpty ((:|)), (<|), head, toList)
import Data.Hashable (Hashable)
import Data.HashSet (HashSet)
import Data.Functor ((<&>))
import Data.Map.Strict (Map)
import Data.Maybe (fromMaybe, maybeToList, catMaybes)
import Data.MonoTraversable (Element, MonoFunctor, oelem, oall, olength, onull, omap, opoint)
import Data.Partition (Partition, discrete, joinElems)
import Data.List (foldl', sort, genericLength)
import Data.Set (Set)
import Data.Vector ((!?))
import GHC.Generics (Generic)
import Optics.Core (AffineFold, (%), preview, set, ix, castOptic, _2, atraversal)
import Optics.Setter (Setter', sets)
import Optics.TH (makePrisms)

import Construction hiding (Deref)
import qualified Construction as C
import qualified Data.Map.Strict as Map
import qualified Data.HashSet as HashSet
import qualified Data.List.NonEmpty as NE
import qualified Data.Partition as Partition

import Prelude hiding (head)

import Debug.Trace (trace, traceM)

----------------------------------
-- Build a usage tree from program


data InterestingUsage
    = DerefUsageKnown Leaf
    | DerefUsageUnknown Int -- ^ size
    | OtherUsage OpUsage
    deriving (Eq, Show, Generic)

data OpUsage
    = OnlySize Int
    deriving (Eq, Show, Generic)
    -- todo: floats, function pointers

data Leaf
    = LReg RegName ExecutionStep
    | LNum Immediate
    | Leaf :+ Leaf
    | Leaf :* Leaf
    deriving (Eq, Show, Generic)

-- | Recursion should only happen in deref, and should reference a leaf bound
-- in deref lhs
data Tree
    = Terminal Leaf
    | Deref (NonEmpty (Leaf, UsageTree))
    deriving (Eq, Show, Generic)

data UsageTree
    = Normal Tree
    | Recursion Leaf
    | SomeUsage OpUsage
    deriving (Eq, Show, Generic)

makePrisms ''Tree
makePrisms ''UsageTree

data Twig
    = Plain Leaf
    | DerefOnce Int Leaf -- ^ size
    deriving (Eq, Show, Generic, Ord)

fromPlain :: Twig -> Maybe Leaf
fromPlain (Plain x) = Just x
fromPlain _ = Nothing

type Equivalence = Partition Twig

-- | A lens for terminals. Allows you to read a leaf and set a tree
type LeafTreeLens = (AffineFold Tree Leaf, Setter' Tree UsageTree)

type instance Element Twig = Leaf
instance MonoFunctor Twig where
    omap f = \case
        Plain x -> Plain (f x)
        DerefOnce s x -> DerefOnce s (f x)

leafMapM :: Applicative m => (Leaf -> m Leaf) -> Leaf -> m Leaf
leafMapM f = \case
    LReg n s -> f $ LReg n s
    LNum n -> f $ LNum n
    l1 :+ l2 -> liftA2 (:+) (leafMapM f l1) (leafMapM f l2)
    l1 :* l2 -> liftA2 (:*) (leafMapM f l1) (leafMapM f l2)
leafForM :: Applicative m => Leaf -> (Leaf -> m Leaf) -> m Leaf
leafForM = flip leafMapM

instance Ord Leaf where
    compare (LNum x) (LNum y) = compare x y
    compare (LNum _) _ = LT
    compare _ (LNum _) = GT

    compare (LReg n1 s1) (LReg n2 s2) = compare (n1, s1) (n2, s2)
    compare (LReg _ _) _ = LT
    compare _ (LReg _ _) = GT

    compare (e1l :+ e1r) (e2l :+ e2r) = compare (e1l, e1r) (e2l, e2r)
    compare (_ :+ _) _ = LT
    compare _ (_ :+ _) = GT

    compare (e1l :* e1r) (e2l :* e2r) = compare (e1l, e1r) (e2l, e2r)

newtype AsUpdate a = AsUpdate a
setUpdate :: Setter' s a -> AsUpdate a -> s -> s
setUpdate lens (AsUpdate x) s = set lens x s


-- | Left for deref, right for simple operand
operandTwig :: ExecutionStep -> Operand -> Twig
operandTwig step = \case
    Imm imm -> Plain $ LNum imm
    RW (Reg name) -> Plain $ LReg name step
    RW (C.Deref modrm) -> fromModRm step modrm
fromModRm :: ExecutionStep -> ModRM -> Twig
fromModRm step =
    let deref = DerefOnce
        plus = (:+)
        mul = (:*)
        reg name = LReg name step
        imm = LNum
    in omap simplify . derefModRmG deref plus mul reg imm
exprLeaf :: TermExpr -> Maybe Leaf
exprLeaf = fmap simplify . \case
    TReg name step -> Just $ LReg name step
    Number n -> Just $ LNum n
    TDeref _ _ -> Nothing
    e1 :+: e2 -> liftA2 (:+) (exprLeaf e1) (exprLeaf e2)
    e1 :*: e2 -> liftA2 (:*) (exprLeaf e1) (exprLeaf e2)

buildTree
    :: SimpleTerm
    -> ExecutionStep
    -> Tree
buildTree (SimpleTerm program termStep termOperand) entry =
    let
      root = case operandTwig termStep termOperand of
            Plain leaf -> Terminal leaf
            DerefOnce size deref -> Deref $ opoint (deref, SomeUsage . OnlySize $ size)
      infos = getSteps program [entry, termStep]
    in goBuild infos root discrete
  where
    goBuild :: [CommandInfo] -> Tree -> Equivalence -> Tree
    goBuild !infos !tree !equivs =
        let
          lenses = gatherLeafs tree
          (tree1, equivs') = foldUpdate infos tree equivs lenses
          tree2 = gatherRecursion tree1 equivs'
        in if trace ("Build step from " <> show tree <> " to " <> show tree2) (tree == tree2)
            then {-trace ("Final equivalences:\n" <> (unlines . map show) (Partition.nontrivialSets equivs')) -}tree
            else goBuild infos tree2 equivs'

-- | Gather all commands reachable from given
getSteps :: Program -> [ExecutionStep] -> [CommandInfo]
getSteps program = getSteps' Map.empty where
    getSteps' :: Map ExecutionStep CommandInfo -> [ExecutionStep] -> [CommandInfo]
    getSteps' !computed !steps =
        let getStep step = if step `member` computed
                then ([], computed)
                else case program !? step of
                    Nothing -> ([], computed)
                    Just command ->
                        let r@CommandInfo {exits} = commandInfo step command
                            computed' = insertMap step r computed
                        in (exits, computed')
        in case steps of
            -- go until each new step is already computed
            [] -> Map.elems computed
            s:ss -> let (newSteps, computed') = getStep s
                    in getSteps' computed' (ss <> newSteps)

gatherLeafs :: Tree -> [LeafTreeLens]
gatherLeafs = \case
    Terminal _ ->
        -- i'm really bending optics here
        -- over o (const v) x = v
        -- was:
        -- sets = id
        -- over (sets id) (const v) x = id (const v) x = const v x = v

        -- need:
        -- over (sets f) (const (Normal v)) x = v
        -- over (sets f) (const _) x = x
        -- f (const (Normal v)) x = v
        -- f (const _) x = x
        let pureNormal f v = case f (error "Please only use optic.over with const") of
                Normal tree -> tree
                _ -> v
        in [(castOptic _Terminal, sets pureNormal)]
    Deref ls -> do
        (usetree, index) <- zip (map snd . toList $ ls) [0,1..]
        subtree <- case usetree of Normal t -> [t]
                                   _ -> []
        (sublensl, sublensr) <- gatherLeafs subtree
        let common = _Deref % ix index % _2 % _Normal
        [(common % sublensl, common % sublensr)]

foldUpdate :: [CommandInfo] -> Tree -> Equivalence
           -> [LeafTreeLens]
           -> (Tree, Equivalence)
foldUpdate _infos !tree !equivs [] = (tree, equivs)
foldUpdate  infos !tree !equivs ((leafLens, treeLens):lenses) =
    case preview leafLens tree of
      -- in previous updates we overwrote this leaf, oh well
      Nothing -> foldUpdate infos tree equivs lenses
      Just leaf ->
          let
            currentWidth = treeWidth tree
            (mbUpdate, equivs') = oneUpdate infos equivs leaf currentWidth
            tree' = case mbUpdate of
              Nothing -> tree
              Just update -> setUpdate treeLens update tree
          in foldUpdate infos tree' equivs' lenses
oneUpdate :: [CommandInfo] -> Equivalence -> Leaf -> Int -> (Maybe (AsUpdate UsageTree), Equivalence)
oneUpdate !infos !equivs !leaf !width =
    let
      equivs1 = getEquivs infos equivs leaf (width + equivsLeeway)
      collect :: (Leaf, UsageTree) -> (Maybe (AsUpdate UsageTree), Equivalence)
      collect derefHead@(l, _) =
            let collection = collectDerefs width infos equivs1 (pure l)
                derefs = derefHead :| map (\(x,y,_) -> (x,y)) collection
                equivs' = last $ equivs1 : map thd3 collection
                derefTree = Deref $ derefs
            in (Just . AsUpdate . Normal $ derefTree, equivs')
    in case interestingUsage infos equivs1 leaf of
        Nothing -> (Nothing, equivs1)
        Just (OtherUsage u) ->
            -- Tree of some usage
            (Just . AsUpdate . SomeUsage $ u, equivs1)
        -- collect deref usage of nearby memory locations as well
        Just (DerefUsageKnown deref0) ->
            let derefHead = (leaf, Normal . Terminal $ deref0)
            in collect derefHead
        Just (DerefUsageUnknown size) ->
            let derefHead = (leaf, SomeUsage . OnlySize $ size)
            in collect derefHead

-- | Assuming base leaf is a dereference, find out if nearby stuff is also dereferenced
collectDerefs :: Int -> [CommandInfo] -> Equivalence
              -> NonEmpty Leaf -- ^ Leafs that we know were dereferenced, in reverse
                               -- order from how the would appear in 'Deref'
              -> [(Leaf, UsageTree, Equivalence)] -- ^ fst is what to deref
collectDerefs 1024 _ _ _ = trace "Too big structure (arbitrary limit)" []
collectDerefs !width !infos !equivs !baseLeafs =
    let
      baseLeaf = head baseLeafs
      -- offsets are ordered to reduce deref noise with something like
      -- accessing far away fields of struct
      candidates = [simplify $ baseLeaf :+ LNum offset | offset <- [1, 2, 4, 8]]
      checkDeref leaf =
        let equivs' = getEquivs infos equivs leaf (width + equivsLeeway)
        in interestingUsage infos equivs' leaf >>= \case
            DerefUsageKnown r -> pure (leaf, Normal . Terminal $ r, equivs')
            DerefUsageUnknown s -> pure (leaf, SomeUsage . OnlySize $ s, equivs')
            OtherUsage _ -> Nothing
      findings = map checkDeref candidates
    in case catMaybes findings of
        [] -> []
        x@(baseLeaf', _, equivs'):_ ->
            -- if we found recurring pattern in derefs, it must be an array
            if baseLeaf' `elem` toList baseLeafs
                then []
                else x : collectDerefs (width+1) infos equivs' (baseLeaf'<|baseLeafs)


equivsLeeway :: Int
equivsLeeway = 2

getEquivs :: [CommandInfo] -> Equivalence -> Leaf -> Int -- ^ max size of leafs
          -> Equivalence
getEquivs !infos !equivs !target !limit =
    let
      targets = Partition.find equivs (Plain target)
      newEquivs = getEquivsOnce infos equivs True (setToList targets)
      goodNew = filter ((<= limit) . twigWidth)
              . filter (not . flip member targets)
              $ newEquivs
    in if onull goodNew
        then equivs
        else let equivs' = foldr (joinElems (Plain target)) equivs newEquivs
             in getEquivs infos equivs' target limit

-- | Perform one full pass of getting new equivalences. Visits each command at
-- most twice: once for explicit equivalences, and once for carries
getEquivsOnce :: [CommandInfo] -> Equivalence
              -> Bool -- ^ Should we collect twigs
              -> [Twig] -> [Twig]
getEquivsOnce infos equivs collectTwigs targets =
    let
      collectExplicit = do
            CommandInfo {equivalences} <- infos
            (eqExpr1, eqExpr2) <- equivalences
            eq1 <- maybeToList . termAsTwig $ eqExpr1
            eq2 <- maybeToList . termAsTwig $ eqExpr2
            (lhs, rhs) <- [(eq1, eq2), (eq2, eq1)]
            original <- targets
            if original == lhs
                then [rhs, omap simplify rhs]
                else []
      collectCarries = do
            CommandInfo {entry, exits, modifications} <- infos
            exit <- exits
            original <- targets
            let modifiedTwigs = map (rwopAsTwig entry) $ modifications
            let advanced = advanceTwig modifiedTwigs entry exit original
            let redvanced = redvanceTwig modifiedTwigs entry exit original
            advanced <> redvanced
      collectSubstructural :: [Twig]
      collectSubstructural = targets >>= \case
            -- for plain is replacing leafs in all sums with equivalents
            {-
            Plain x -> map Plain . leafForM x $ \leaf ->
                -- simple replacement: don't perform new searches
                map simplify . catMaybes . map fromPlain . setToList . Partition.find equivs $ (Plain leaf)
            -} -- that would be nice, but it allocated just too fucking much
            Plain (LNum n :+ LReg r s) -> fmap (\x -> Plain $ LNum n :+ x) $
                let fromReg = \case
                        Plain (LReg name step) -> Just $ LReg name step
                        _ -> Nothing
                in catMaybes . map fromReg . setToList . Partition.find equivs $ Plain (LReg r s)
            Plain _ -> []
            -- for derefs: replace this modrm with equivalents
            DerefOnce size x -> if not collectTwigs then [] else do
                let subTargets = Partition.find equivs (Plain x)
                subResult <- getEquivsOnce infos equivs False (setToList subTargets)
                plain <- maybeToList . fromPlain $ subResult
                pure $ DerefOnce size plain
    in collectExplicit <> collectCarries <> collectSubstructural
    where
        advanceLeaf :: [Leaf] -> ExecutionStep -> ExecutionStep -> Leaf -> [Leaf]
        advanceLeaf modified from to leaf = leafForM leaf $ guardNotIn modified >=> \case
            LReg name step -> do
                guard $ step == from
                [LReg name from, LReg name to]
            x -> pure x
        -- Works like advanceLeaf, but the non-modified check is after obtaining the new leaf, and we check regs for destination not source
        redvanceLeaf :: [Leaf] -> ExecutionStep -> ExecutionStep -> Leaf -> [Leaf]
        redvanceLeaf modified from to leaf = leafForM leaf $ \case
            LReg name step -> do
                guard $ step == to
                guardNotIn modified =<< [LReg name from, LReg name to]
            x -> guardNotIn modified x
        --
        advanceTwig modified from to twig = guardNotIn modified twig >>
            let modifiedLeafs = catMaybes . map fromPlain $ modified
            in case twig of
                Plain leaf -> Plain <$> advanceLeaf modifiedLeafs from to leaf
                DerefOnce s leaf -> DerefOnce s <$> advanceLeaf modifiedLeafs from to leaf
        redvanceTwig modified from to twig =
            let modifiedLeafs = catMaybes . map fromPlain $ modified
            in guardNotIn modified =<< case twig of
                Plain leaf -> Plain <$> redvanceLeaf modifiedLeafs from to leaf
                DerefOnce s leaf -> DerefOnce s <$> redvanceLeaf modifiedLeafs from to leaf
        --
        guardNotIn :: (Alternative m, Eq a) => [a] -> a -> m a
        guardNotIn xs x = do
            guard . not $ x `elem` xs
            pure x


-- dereferences, sizes, floats
interestingUsage :: [CommandInfo] -> Equivalence -> Leaf -> Maybe InterestingUsage
interestingUsage infos equivs target =
    let
      sizeDerefs = do
            CommandInfo {entry, dereferenced} <- infos
            DerefOnce size operand <- fromModRm entry <$> dereferenced
            if equivalentIn equivs (Plain target) (Plain operand)
                then [DerefUsageUnknown size]
                else []
      eqDerefs = do
            CommandInfo {equivalences} <- infos
            (eq1, eq2) <- equivalences
            (lhs, rhs) <- [(eq1, eq2), (eq2, eq1)]
            case lhs of
                TDeref expr _ -> maybeToList $ do
                    derefed <- exprLeaf expr
                    if equivalentIn equivs (Plain target) (Plain derefed)
                        then DerefUsageKnown <$> exprLeaf rhs
                        else Nothing
                _ -> []
    in case eqDerefs <> sizeDerefs of
        x:_ -> Just x
        [] -> Nothing

-- | Find if tree has recursive binding and replace it with Recursion
gatherRecursion :: Tree -> Equivalence -> Tree
gatherRecursion tree equivs = unNormal $ gather [] (Normal tree) where
    gather bottoms = \case
        Normal (Terminal x) -> case findIn equivs bottoms (Plain $ simplify x) of
            Just (Plain y) -> Recursion y
            Just _ -> error "shouldn't happen"
            Nothing -> Normal . Terminal $ x
        Normal (Deref derefs) ->
            let
              (parents, children) = unzip . toList $ derefs
              bottoms' = (map Plain parents) <> bottoms
              children' = map (gather bottoms') children
            in Normal . Deref . NE.fromList $ zip parents children'
        Recursion x -> Recursion x
        SomeUsage s -> SomeUsage s
    unNormal (Normal t) = t
    unNormal _ = error "Logic error in gather: Normal root transformed"

findIn :: (Ord a, Show a) => Partition a -> [a] -> a -> Maybe a
findIn _ [] _ = Nothing
findIn equivs (x:xs) y =
    if equivalentIn equivs x y
        then Just x
        else findIn equivs xs y

-- | Commutes arithmetics so that simpler expressions are on the left, also
-- performs arithmetic on numbers
simplify :: Leaf -> Leaf
simplify = \case
    LNum (!n1) :+ LNum (!n2) -> LNum (n1 + n2)
    LNum (!n1) :* LNum (!n2) -> LNum (n1 * n2)
    LNum (!n1) :+ (LNum (!n2) :+ (!x3)) -> LNum (n1 + n2) :+ x3
    LNum (!n1) :* (LNum (!n2) :* (!x3)) -> LNum (n1 * n2) :* x3
    -- Move lesser (simpler) terms to the left and simplify again
    (!x1) :+ (!x2) | x1 <= x2  -> simplify x1 :+ simplify x2
                   | otherwise -> simplify $ simplify x2 :+ simplify x1
    (!x1) :* (!x2) | x1 <= x2  -> simplify x1 :* simplify x2
                   | otherwise -> simplify $ simplify x2 :* simplify x1
    x -> x

treeWidth :: (Ord a, Num a) => Tree -> a
treeWidth = \case
    Terminal _ -> 1
    Deref ds ->
        let currentLen = fromIntegral . olength $ ds
            normal (Normal t) = Just t
            normal _ = Nothing
            recLen = maximum . map treeWidth . catMaybes
                   . map (normal . snd)
                   . toList
                   $ ds
        in max currentLen recLen

twigWidth :: Num a => Twig -> a
twigWidth = \case
    Plain x -> leafWidth x
    DerefOnce _ x -> 1 + leafWidth x -- not sure of semantics, maybe just 1?
leafWidth :: Num a => Leaf -> a
leafWidth = \case
    LReg _ _ -> 1
    LNum _ -> 1
    LNum n :+ l2 -> fromIntegral (n `div` 8) + leafWidth l2
    LNum n :* l2 -> fromIntegral (n `div` 8) * leafWidth l2
    l1 :+ l2 -> leafWidth l1 + leafWidth l2
    l1 :* l2 -> leafWidth l1 + leafWidth l2

termAsTwig :: TermExpr -> Maybe Twig
termAsTwig = \case
    TDeref ex size -> pure . (DerefOnce size) =<< termAsLeaf ex
    x -> Plain <$> termAsLeaf x

termAsLeaf :: TermExpr -> Maybe Leaf
termAsLeaf = \case
    TReg name step -> Just $ LReg name step
    Number n -> Just $ LNum n
    TDeref _ _ -> Nothing
    e1 :+: e2 -> liftA2 (:+) (termAsLeaf e1) (termAsLeaf e2)
    e1 :*: e2 -> liftA2 (:*) (termAsLeaf e1) (termAsLeaf e2)
rwopAsTwig :: ExecutionStep -> RWOperand -> Twig
rwopAsTwig step = \case
    Reg name -> Plain $ LReg name step
    C.Deref modrm -> fromModRm step modrm


thd3 :: (a, b, c) -> c
thd3 (_, _, x) = x

equivalentIn :: Ord a => Partition a -> a -> a -> Bool
equivalentIn p x y = Partition.rep p x == Partition.rep p y


sizeOf :: Leaf -> Int
sizeOf = \case
    LReg name _ -> sizeOfReg name
    LNum _ -> 8
    l1 :+ l2 -> max (sizeOf l1) (sizeOf l2)
    l1 :* l2 -> max (sizeOf l1) (sizeOf l2)

sizeOfReg :: RegName -> Int
sizeOfReg ('r':_) = 8
sizeOfReg ('e':_) = 4
sizeOfReg x | olength x == 2  = case last x of
    'x' -> 2
    'l' -> 1
    'h' -> 1
    _ -> 8
sizeOfReg _ = 8


------------------------------------------------
-- Reduce the tree built to a user readable type

treeAsType :: Tree -> AsmType
treeAsType baseTree = fst $ go [] 0 baseTree where
    -- which operands start recursion
    recBases :: [Leaf]
    recBases = get (Normal baseTree)
        where get = \case
                    Recursion x -> [x]
                    Normal (Infer.Deref xs) -> concatMap (get . snd) xs
                    _ -> []
    --
    go :: [(Leaf, TypeVar)] -> Integer -> Tree -> (AsmType, Integer)
    go !binds !lastTvar = \case
        Terminal leaf -> (TInt $ 8 * sizeOf leaf, lastTvar)
        Deref xs ->
            let
              presentBases = filter (`elem` recBases) . map fst . toList $ xs
              lastTvar' = if null presentBases then lastTvar else lastTvar + 1
              newTvar = "X" <> show lastTvar
              binds' = binds <> zip presentBases (repeat newTvar)
              prefix = if onull presentBases
                        then TPtr . Struct
                        else Mu newTvar . TPtr . Struct
            in first prefix
             . foldDerefs binds' lastTvar'
             . NE.map snd -- here we throw away offset information sadly
             $ xs
    -- like mapping a deref to a struct, but it's not elementwise independent
    -- because we have to pass typevar info through
    foldDerefs :: [(Leaf, TypeVar)] -> Integer -> NonEmpty UsageTree
               -> (NonEmpty AsmType, Integer)
    foldDerefs !binds !lastTvar derefs =
        let toType (tree, ltvar) = case tree of
             Recursion x -> case lookup x binds of
                Just b -> (TPtr . TTypeVar $ b, ltvar)
                Nothing -> (TTypeVar "!bind not found!", ltvar)
             SomeUsage (OnlySize size) ->
                (TInt $ size * 8, ltvar)
             Normal t -> go binds ltvar t
        in mapContext toType lastTvar derefs
    mapContext :: ((a, c) -> (b, c)) -> c -> NonEmpty a -> (NonEmpty b, c)
    mapContext f c (x :| xs) =
        let (y, c') = f (x, c)
            (ys, cEnd) = case unzip $ mapContext' f c' xs of
                (ys', []) -> (ys', c')
                (ys', ce:ces) -> (ys', NE.last (ce:|ces))
        in (y :| ys, cEnd)
    mapContext' :: ((a, c) -> (b, c)) -> c -> [a] -> [(b, c)]
    mapContext' _ _ [] = []
    mapContext' f c (x:xs) = let (y, c') = f (x, c)
                             in (y, c') : mapContext' f c' xs


treeAsTerm :: Tree -> TermExpr
treeAsTerm = \case
    Terminal l -> leafAsTerm l
    Deref ((l, t) :| _) -> TDeref (leafAsTerm l) (sizeOfTree t)
  where
    leafAsTerm = \case
        LReg n s -> TReg n s
        LNum n -> Number n
        l1 :+ l2 -> leafAsTerm l1 :+: leafAsTerm l2
        l1 :* l2 -> leafAsTerm l1 :*: leafAsTerm l2
    sizeOfTree = \case
        Normal (Terminal t) -> sizeOf t
        Normal (Deref _) -> 8 -- ptr obviously
        Recursion _ -> 8 -- rec is ptr
        SomeUsage (OnlySize s) -> s

